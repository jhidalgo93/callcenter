package co.com.callcenter.controller;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.callcenter.TestConfigurationRunner;
import co.com.callcenter.conf.LlamadaConf;
import co.com.callcenter.model.Empleado;
import co.com.callcenter.model.Llamada;
import co.com.callcenter.model.RolEmpleado;
import co.com.callcenter.service.EmpleadoService;
import co.com.callcenter.service.LlamadaService;
import co.com.callcenter.util.EstadoEmpleadoEnum;
import co.com.callcenter.util.EstadoLlamadaEnum;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfigurationRunner.class)
@TestPropertySource(properties = "app.scheduling.enable=false")
public class DispatcherTest {

	@TestConfiguration
	static class DispatcherTestContextConfiguration {
		@Bean
		public Dispatcher dispatcher() {
			return new Dispatcher();
		}

	}

	@Autowired
	private LlamadaConf llamadaConf;
	@Autowired
	private Dispatcher dispatcher;

	@MockBean(name = "llamadaService")
	private LlamadaService llamadaService;

	@MockBean(name = "empleadoService")
	private EmpleadoService empleadoService;

	private ExecutorService executor = Executors.newFixedThreadPool(1);

	/**
	 * Metodo encargado de validar el flujo cuando no se encuentran nigun resultado
	 * para empleados
	 */
	@Test
	public void testNoResultEmpleados() {
		try {
			dispatcher.setIniciarAatencion(true);
			List<Empleado> empleados = new ArrayList<Empleado>();
			Mockito.when(empleadoService.buscarEmpleadosDisponibles()).thenReturn(empleados);
			executor.execute(dispatcher);

			Thread.sleep(2000);
			dispatcher.setIniciarAatencion(false);

			Field fieldEmpleados = dispatcher.getClass().getDeclaredField("empleadosDisponibles");
			fieldEmpleados.setAccessible(true);
			List<Empleado> resultados = (List<Empleado>) fieldEmpleados.get(dispatcher);
			Assert.assertTrue(resultados.size() == 0 ? true : false);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException | SecurityException
				| InterruptedException e) {
			Assert.assertTrue(false);
			dispatcher.setIniciarAatencion(false);
		}

	}

	/**
	 * Metodo encargado de validar el flujo para cuando se encuentran resultado de
	 * empleados y no se encuentra resultados de llamadas
	 */
	@Test
	public void testNoResultLlamadas() {
		try {
			dispatcher.setIniciarAatencion(true);
			List<Empleado> empleados = new ArrayList<Empleado>();

			cargarEmpleado(empleados);

			Mockito.when(empleadoService.buscarEmpleadosDisponibles()).thenReturn(empleados);

			List<Llamada> listLlamadas = new ArrayList<Llamada>();
			Mockito.when(llamadaService.buscarLlamadaEnEspera(llamadaConf.getMaximo_de_atenciones(), 1))
					.thenReturn(listLlamadas);

			executor.execute(dispatcher);

			Thread.sleep(2000);
			dispatcher.setIniciarAatencion(false);

			Field fieldLlamadas = dispatcher.getClass().getDeclaredField("llamadasEnEspera");
			fieldLlamadas.setAccessible(true);
			List<Llamada> resultados = (List<Llamada>) fieldLlamadas.get(dispatcher);
			Assert.assertTrue(resultados.size() == 0 ? true : false);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException | SecurityException
				| InterruptedException e) {
			Assert.assertTrue(false);
			dispatcher.setIniciarAatencion(false);
		}

	}

	@Test
	public void test10Llamadas() {
		try {
			dispatcher.setIniciarAatencion(true);
			List<Empleado> empleados = new ArrayList<Empleado>();

			cargarEmpleado(empleados);

			Mockito.when(empleadoService.buscarEmpleadosDisponibles()).thenReturn(empleados);
			Mockito.when(empleadoService.filtrarEmpleadoDisponible(empleados)).thenReturn(empleados.get(0));

			List<Llamada> llamadasEntrantes = new ArrayList<Llamada>();
			cargarLlamadas(llamadasEntrantes, 10);

			Mockito.when(llamadaService.buscarLlamadaEnEspera(llamadaConf.getMaximo_de_atenciones(), 12))
					.thenReturn(llamadasEntrantes);

			executor.execute(dispatcher);

			Thread.sleep(2000);
			dispatcher.setIniciarAatencion(false);

			Field fieldLlamadas = dispatcher.getClass().getDeclaredField("llamadasEnEspera");
			fieldLlamadas.setAccessible(true);
			List<Llamada> resultados = (List<Llamada>) fieldLlamadas.get(dispatcher);
			Assert.assertTrue(resultados.size() == 0 ? true : false);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException | SecurityException
				| InterruptedException e) {
			Assert.assertTrue(false);
			dispatcher.setIniciarAatencion(false);
		}

	}

	private void cargarLlamadas(List<Llamada> llamadasEntrantes, int cantidad) {
		for (int i = 0; i < cantidad; i++) {
			Llamada llamada = new Llamada();
			llamada.setId(Long.valueOf(i));
			llamada.setEstado(EstadoLlamadaEnum.EN_ESPERA.getId());
			llamada.setFechaIngreso(new Date());
			llamada.setTelefono("32111" + i);
			llamadasEntrantes.add(llamada);
		}
	}

	/**
	 * Metodo encargado de mockiar un empleado
	 * 
	 * @param empleados
	 */
	private void cargarEmpleado(List<Empleado> empleados) {

		for (int i = 0; i < 4; i++) {
			Empleado empleado = new Empleado();
			empleado.setNombre("Operador " + i);
			empleado.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
			empleado.setId(Long.valueOf(i));
			empleado.setIdentificacion("343434" + i);
			RolEmpleado rol = new RolEmpleado();
			rol.setId(1L);
			rol.setNivelResponsabilidad(1L);
			rol.setDescripcion("Pepito - " + i);
			empleado.setRol(rol);

			Empleado empleado2 = new Empleado();
			empleado2.setNombre("Supervisor " + i);
			empleado2.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
			empleado2.setId(Long.valueOf(i));
			empleado2.setIdentificacion("343434" + i);
			RolEmpleado rol2 = new RolEmpleado();
			rol2.setId(1L);
			rol2.setNivelResponsabilidad(1L);
			rol2.setDescripcion("Maria - " + i);
			empleado.setRol(rol2);

			Empleado empleado3 = new Empleado();
			empleado3.setNombre("Director " + i);
			empleado3.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
			empleado3.setId(Long.valueOf(i));
			empleado3.setIdentificacion("343434" + i);
			RolEmpleado rol3 = new RolEmpleado();
			rol3.setId(1L);
			rol3.setNivelResponsabilidad(1L);
			rol3.setDescripcion("Lorena - " + i);
			empleado.setRol(rol3);

			empleados.add(empleado);
			empleados.add(empleado2);
			empleados.add(empleado3);
		}

	}

}