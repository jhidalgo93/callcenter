package co.com.callcenter.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.callcenter.TestConfigurationRunner;
import co.com.callcenter.model.Empleado;
import co.com.callcenter.model.Llamada;
import co.com.callcenter.model.RolEmpleado;
import co.com.callcenter.repository.EmpleadoRepository;
import co.com.callcenter.repository.LLamadaRepository;
import co.com.callcenter.repository.LlamadaXEmpleadoRepository;
import co.com.callcenter.service.impl.EmpleadoServiceImpl;
import co.com.callcenter.util.EstadoEmpleadoEnum;
import co.com.callcenter.util.EstadoLlamadaEnum;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfigurationRunner.class)
@TestPropertySource(properties = "app.scheduling.enable=false")
public class EmpleadoServiceTest {

	@TestConfiguration
	static class EmpleadoServiceTestContextConfiguration {
		@Bean
		public EmpleadoService empleadoService() {
			return new EmpleadoServiceImpl();
		}

	}

	@Autowired
	private EmpleadoService empleadoService;

	@MockBean
	private EmpleadoRepository empleadoRepository;

	@MockBean
	private LlamadaXEmpleadoRepository llamadaXEmpleadoRepository;

	@MockBean
	private LLamadaRepository llamadaRepository;


	@Test
	public void buscarEmpleadosDisponiblesTest() {
		Mockito.when(empleadoRepository.buscarEmpleadosDisponibles()).thenReturn(cargarEmpleado(true, true, true));

		List<Empleado> resultado = empleadoService.buscarEmpleadosDisponibles();
		Assert.assertTrue(resultado.size() > 0 ? true : false);
	}

	@Test
	public void filtrarEmpleadoDisponibleOperador() {

		Empleado empleadoOperador = empleadoService.filtrarEmpleadoDisponible(cargarEmpleado(true, true, true));
		Assert.assertTrue(empleadoOperador.getRol().getId().equals(1L) ? true : false);

	}

	@Test
	public void filtrarEmpleadoDisponibleSupervisor() {

		Empleado empleadoOperador = empleadoService.filtrarEmpleadoDisponible(cargarEmpleado(false, true, true));
		Assert.assertTrue(empleadoOperador.getRol().getId().equals(2L) ? true : false);

	}

	@Test
	public void filtrarEmpleadoDisponibleDirector() {

		Empleado empleadoOperador = empleadoService.filtrarEmpleadoDisponible(cargarEmpleado(false, false, true));
		Assert.assertTrue(empleadoOperador.getRol().getId().equals(3L) ? true : false);

	}

	@Test
	public void atenderLlamada() {

		Empleado empleado = new Empleado();
		empleado.setNombre("Operador ");
		empleado.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
		empleado.setId(1L);
		empleado.setIdentificacion("343434");
		RolEmpleado rol = new RolEmpleado();
		rol.setId(1L);
		rol.setNivelResponsabilidad(1L);
		rol.setDescripcion("Pepito - ");
		empleado.setRol(rol);

		Llamada llamada = new Llamada();
		llamada.setId(1L);
		llamada.setEstado(EstadoLlamadaEnum.EN_ESPERA.getId());
		llamada.setFechaIngreso(new Date());
		llamada.setTelefono("32111");

		try {
			empleadoService.atenderLlamada(empleado, llamada);
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.assertTrue(false);
		}
	}

	/**
	 * Metodo encargado de mockiar un empleado
	 * 
	 * @param empleados
	 */
	private List<Empleado> cargarEmpleado(boolean operador, boolean supervisor, boolean director) {

		List<Empleado> empleados = new ArrayList<Empleado>();
		for (int i = 0; i < 4; i++) {
			if (operador) {
				Empleado empleado = new Empleado();
				empleado.setNombre("Operador " + i);
				empleado.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
				empleado.setId(Long.valueOf(i));
				empleado.setIdentificacion("343434" + i);
				RolEmpleado rol = new RolEmpleado();
				rol.setId(1L);
				rol.setNivelResponsabilidad(1L);
				rol.setDescripcion("Pepito - " + i);
				empleado.setRol(rol);
				empleados.add(empleado);
			}
			if (supervisor) {
				Empleado empleado2 = new Empleado();
				empleado2.setNombre("Supervisor " + i);
				empleado2.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
				empleado2.setId(Long.valueOf(i));
				empleado2.setIdentificacion("343434" + i);
				RolEmpleado rol2 = new RolEmpleado();
				rol2.setId(2L);
				rol2.setNivelResponsabilidad(2L);
				rol2.setDescripcion("Maria - " + i);
				empleado2.setRol(rol2);
				empleados.add(empleado2);
			}

			if (director) {
				Empleado empleado3 = new Empleado();
				empleado3.setNombre("Director " + i);
				empleado3.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
				empleado3.setId(Long.valueOf(i));
				empleado3.setIdentificacion("343434" + i);
				RolEmpleado rol3 = new RolEmpleado();
				rol3.setId(3L);
				rol3.setNivelResponsabilidad(3L);
				rol3.setDescripcion("Lorena - " + i);
				empleado3.setRol(rol3);
				empleados.add(empleado3);
			}

		}
		return empleados;

	}

}
