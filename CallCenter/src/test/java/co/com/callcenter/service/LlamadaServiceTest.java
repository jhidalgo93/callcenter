package co.com.callcenter.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.callcenter.TestConfigurationRunner;
import co.com.callcenter.model.Llamada;
import co.com.callcenter.repository.LLamadaRepository;
import co.com.callcenter.service.impl.LlamadaServiceImpl;
import co.com.callcenter.util.EstadoLlamadaEnum;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfigurationRunner.class)
@TestPropertySource(properties = "app.scheduling.enable=false")
public class LlamadaServiceTest {

	@TestConfiguration
	static class LlamadaServiceTestContextConfiguration {
		@Bean
		public LlamadaService llamadaService() {
			return new LlamadaServiceImpl();
		}

	}

	@Autowired
	private LlamadaService llamadaService;

	@MockBean
	private LLamadaRepository llamadaRepository;

	@Test
	public void crearLLamadasTest() {
		try {
			llamadaService.crearLLamadas(crearLlamadas());
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.assertTrue(false);
		}

	}

	@Test
	public void buscarLlamadaEnEsperaTest() {

		Mockito.when(llamadaRepository.buscarLlamadaEnEspera(5, 1L)).thenReturn(crearLlamadas());

		try {
			llamadaService.buscarLlamadaEnEspera(10, 5);
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.assertTrue(false);
		}

	}

	private List<Llamada> crearLlamadas() {
		Llamada llamada = new Llamada();
		llamada.setId(1L);
		llamada.setEstado(EstadoLlamadaEnum.EN_ESPERA.getId());
		llamada.setFechaIngreso(new Date());
		llamada.setTelefono("32111");

		Llamada llamada2 = new Llamada();
		llamada2.setId(1L);
		llamada2.setEstado(EstadoLlamadaEnum.EN_ESPERA.getId());
		llamada2.setFechaIngreso(new Date());
		llamada2.setTelefono("3211155");

		List<Llamada> llamadas = new ArrayList<Llamada>();
		llamadas.add(llamada);
		llamadas.add(llamada2);
		return llamadas;
	}

}
