package co.com.callcenter.repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.callcenter.TestConfigurationRunner;
import co.com.callcenter.model.Empleado;
import co.com.callcenter.model.RolEmpleado;
import co.com.callcenter.util.EstadoEmpleadoEnum;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = TestConfigurationRunner.class)
@TestPropertySource(properties = "app.scheduling.enable=false")
public class EmpleadoRepositoryTest {

	@Autowired
	private EmpleadoRepository empleadoRepository;

	@Test
	public void buscarEmpleadosDisponibles() {

		Empleado empleado = new Empleado();
		empleado.setNombre("Operador ");
		empleado.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
		empleado.setId(1L);
		empleado.setIdentificacion("343434");
		RolEmpleado rol = new RolEmpleado();
		rol.setId(1L);
		empleado.setRol(rol);

		empleadoRepository.save(empleado);

		List<Empleado> resultado = empleadoRepository.buscarEmpleadosDisponibles();
		Assert.assertTrue(resultado != null && resultado.size() > 0);

	}

}
