package co.com.callcenter.repository;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.callcenter.TestConfigurationRunner;
import co.com.callcenter.model.Llamada;
import co.com.callcenter.util.EstadoLlamadaEnum;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = TestConfigurationRunner.class)
@TestPropertySource(properties = "app.scheduling.enable=false")
public class LlamadaRepositoryTest {

	@Autowired
	private LLamadaRepository llamadaRepository;

	@Test
	public void buscarLlamadaEnEsperaTest() {

		Llamada llamada = new Llamada();
		llamada.setId(1L);
		llamada.setEstado(EstadoLlamadaEnum.EN_ESPERA.getId());
		llamada.setFechaIngreso(new Date());
		llamada.setTelefono("32111");

		llamadaRepository.save(llamada);

		List<Llamada> resultado = llamadaRepository.buscarLlamadaEnEspera(10, 1L);
		Assert.assertTrue(resultado != null && resultado.size() > 0);

	}

}
