package co.com.callcenter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import co.com.callcenter.model.Llamada;

public interface LLamadaRepository extends CrudRepository<Llamada, Long> {

	@Query(value = "SELECT * FROM (SELECT * FROM LLAMADA WHERE ESTADO = :espera order by FECHA_INGRESO ASC) LIMIT :limite", nativeQuery = true)
	List<Llamada> buscarLlamadaEnEspera(@Param("limite") int limite, @Param("espera") Long espera);


}
