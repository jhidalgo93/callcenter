package co.com.callcenter.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.callcenter.model.LlamadaXEmpleado;
import co.com.callcenter.model.LlamadaXEmpleadoPK;

public interface LlamadaXEmpleadoRepository extends CrudRepository<LlamadaXEmpleado, LlamadaXEmpleadoPK> {

	
	
}
