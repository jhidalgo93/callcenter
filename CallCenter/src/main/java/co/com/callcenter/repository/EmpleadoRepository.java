package co.com.callcenter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import co.com.callcenter.model.Empleado;

public interface EmpleadoRepository extends CrudRepository<Empleado, Long> {

	@Query(value = "SELECT * FROM EMPLEADO WHERE ESTADO = 1", nativeQuery = true)
	List<Empleado> buscarEmpleadosDisponibles();

}
