package co.com.callcenter.service;

import java.util.List;

import co.com.callcenter.model.Llamada;

public interface LlamadaService {

	void crearLLamadas(List<Llamada> listLLamadas);

	List<Llamada> buscarLlamadaEnEspera(int numeroMaximoAtenciones,
			int numeroEmpleadosDisponibles);

}
