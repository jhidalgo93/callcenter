package co.com.callcenter.service;

import java.util.List;

import co.com.callcenter.model.Empleado;
import co.com.callcenter.model.Llamada;

/**
 * Interface que hace referencia a los metodos que puede tener el empleado
 * 
 * @author Juan Carlos Hidalgo Polo
 *
 */
public interface EmpleadoService {

	List<Empleado> buscarEmpleadosDisponibles();

	Empleado filtrarEmpleadoDisponible(List<Empleado> empleadosDisponibles);

	void atenderLlamada(Empleado empleadoDisponible, Llamada llamada);

}
