package co.com.callcenter.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.callcenter.conf.LlamadaConf;
import co.com.callcenter.model.Empleado;
import co.com.callcenter.model.Llamada;
import co.com.callcenter.model.LlamadaXEmpleado;
import co.com.callcenter.model.LlamadaXEmpleadoPK;
import co.com.callcenter.repository.EmpleadoRepository;
import co.com.callcenter.repository.LLamadaRepository;
import co.com.callcenter.repository.LlamadaXEmpleadoRepository;
import co.com.callcenter.service.EmpleadoService;
import co.com.callcenter.util.EstadoEmpleadoEnum;
import co.com.callcenter.util.EstadoLlamadaEnum;

/**
 * Clase encargada de gestionar los procesos del empleado
 * 
 * @author Juan Carlos Hidalgo Polo
 *
 */
@Service
public class EmpleadoServiceImpl implements EmpleadoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoServiceImpl.class);

	@Autowired
	private EmpleadoRepository empleadoRepository;

	@Autowired
	private LlamadaXEmpleadoRepository llamadaXEmpleadoRepository;

	@Autowired
	private LLamadaRepository llamadaRepository;

	@Autowired
	private LlamadaConf llamadaConf;

	private static final int MILISECONDS = 1000;

	/**
	 * Metodo encargado de buscar los empleados disponibles
	 */
	@Override
	public List<Empleado> buscarEmpleadosDisponibles() {

		List<Empleado> empleados = null;
		try {
			empleados = empleadoRepository.buscarEmpleadosDisponibles();
		} catch (NoResultException e) {
			LOGGER.error(e.getMessage());
		}
		return empleados;
	}

	/**
	 * Metodo encargado de filtrar por nivel de cargo los empleados disponibles para
	 * atender una llamada
	 */
	@Override
	@Transactional
	public Empleado filtrarEmpleadoDisponible(List<Empleado> empleadosDisponibles) {

		Empleado empleado = null;
		if (empleadosDisponibles != null && empleadosDisponibles.size() > 0) {

			try {

				// filtramos la lista de empleados por el nivel de responsabilidad del empleado
				List<Empleado> empleadosFiltrados = empleadosDisponibles.stream().filter(e -> e.getRol()
						.getNivelResponsabilidad() == (empleadosDisponibles.stream().map(em -> em.getRol().getId())
								.collect(Collectors.toList()).stream().mapToLong(n -> n).min().getAsLong()))
						.collect(Collectors.toList());

				// Obtenemos la cantidad de empleados para poder realizar un random y
				// seleccionar un empleado al azar
				int numeroEmpleados = empleadosFiltrados.size() - 1;

				Random r = new Random();
				int empleadoSeleccionar = r.nextInt((numeroEmpleados - 0) + 1) + 0;

				empleado = empleadosFiltrados.get(empleadoSeleccionar);
				empleado.setEstado(EstadoEmpleadoEnum.OCUPADO.getId());
				empleadoRepository.save(empleado);

				// Generamos un radom para seleccionar un empleado
			} catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
				LOGGER.error(e.getMessage());
			}
		}

		return empleado;
	}

	/**
	 * Metodo Async el cual va a realizar la atencion de la llamada de forma
	 * asyncrona
	 */
	@Async
	@Transactional
	@Override
	public void atenderLlamada(Empleado empleadoDisponible, Llamada llamada) {
		Long tiempoEstimadoLLamada = (long) new Random()
				.nextInt((llamadaConf.getDuracion_maxima() - llamadaConf.getDuracion_minima()) + 1)
				+ llamadaConf.getDuracion_minima();

		try {
			// Creamos el registro de la llamada
			LlamadaXEmpleado llamadaXEmpleado = new LlamadaXEmpleado();

			LlamadaXEmpleadoPK id = new LlamadaXEmpleadoPK();
			id.setEmpleado_id(empleadoDisponible);
			id.setLlamada_id(llamada);

			llamadaXEmpleado.setId(id);
			llamadaXEmpleado.setDuracionLlamada(tiempoEstimadoLLamada);
			llamadaXEmpleado.setFechaAtencion(new Date());
			// Ahora agregamos una emulacion de tiempo de la llamada
			Thread.sleep(tiempoEstimadoLLamada * MILISECONDS);

			// pasamos actualizar el estado de la llamada a finalizada

			llamada.setEstado(EstadoLlamadaEnum.FINALIZADA.getId());

			llamadaXEmpleadoRepository.save(llamadaXEmpleado);
			llamadaRepository.save(llamada);

			// Indicamos que el empleado esta libre
			empleadoDisponible.setEstado(EstadoEmpleadoEnum.LIBRE.getId());
			empleadoRepository.save(empleadoDisponible);

		} catch (InterruptedException e) {
			LOGGER.error(e.getMessage());
		}
	}

}
