package co.com.callcenter.service.impl;

import java.util.List;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.callcenter.model.Llamada;
import co.com.callcenter.repository.LLamadaRepository;
import co.com.callcenter.service.LlamadaService;
import co.com.callcenter.util.EstadoLlamadaEnum;

@Service
@Transactional
public class LlamadaServiceImpl implements LlamadaService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LlamadaServiceImpl.class);

	@Autowired
	private LLamadaRepository llamadaRepository;

	@Override
	public void crearLLamadas(List<Llamada> listLLamadas) {
		listLLamadas.forEach((llamada) -> llamadaRepository.save(llamada));
	}


	@Override
	public List<Llamada> buscarLlamadaEnEspera(int numeroMaximoAtenciones, int numeroEmpleadosDisponibles) {

		List<Llamada> llamadas = null;

		int limite = calcularCapacidadPermitida(numeroMaximoAtenciones, numeroEmpleadosDisponibles);
		try {
			llamadas = llamadaRepository.buscarLlamadaEnEspera(limite, EstadoLlamadaEnum.EN_ESPERA.getId());
			llamadas.forEach(llamada -> llamada.setEstado(EstadoLlamadaEnum.EN_PROGRESO.getId()));
			llamadaRepository.saveAll(llamadas);
		} catch (NoResultException e) {
			LOGGER.error(e.getMessage());
		}
		return llamadas;
	}

	/**
	 * Metodo encargado de calcular la capacidad permitida de atencion de llamadas
	 * 
	 * @param numeroProcesosActivos
	 * @param numeroMaximoAtenciones
	 * @param numeroEmpleadosDisponibles
	 * @return
	 */
	private int calcularCapacidadPermitida(int numeroMaximoAtenciones, int numeroEmpleadosDisponibles) {
		return numeroMaximoAtenciones <= numeroEmpleadosDisponibles ? numeroMaximoAtenciones
				: numeroEmpleadosDisponibles;
	}

}
