package co.com.callcenter.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class LlamadaXEmpleadoPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "llamada_id")
	private Llamada llamada_id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empleado_id")
	private Empleado empleado_id;

	public Llamada getLlamada_id() {
		return llamada_id;
	}

	public void setLlamada_id(Llamada llamada_id) {
		this.llamada_id = llamada_id;
	}

	public Empleado getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Empleado empleado_id) {
		this.empleado_id = empleado_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empleado_id == null) ? 0 : empleado_id.hashCode());
		result = prime * result + ((llamada_id == null) ? 0 : llamada_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LlamadaXEmpleadoPK other = (LlamadaXEmpleadoPK) obj;
		if (empleado_id == null) {
			if (other.empleado_id != null)
				return false;
		} else if (!empleado_id.equals(other.empleado_id))
			return false;
		if (llamada_id == null) {
			if (other.llamada_id != null)
				return false;
		} else if (!llamada_id.equals(other.llamada_id))
			return false;
		return true;
	}

}
