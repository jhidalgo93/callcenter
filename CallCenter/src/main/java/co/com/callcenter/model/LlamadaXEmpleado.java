package co.com.callcenter.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "LLAMADA_X_EMPLEADO")
public class LlamadaXEmpleado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LlamadaXEmpleadoPK id;

	@Column(name = "fecha")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAtencion;

	@Column(name = "duracion_llamada")
	private Long duracionLlamada;

	public LlamadaXEmpleadoPK getId() {
		return id;
	}

	public void setId(LlamadaXEmpleadoPK id) {
		this.id = id;
	}

	public Date getFechaAtencion() {
		return fechaAtencion;
	}

	public void setFechaAtencion(Date fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public Long getDuracionLlamada() {
		return duracionLlamada;
	}

	public void setDuracionLlamada(Long duracionLlamada) {
		this.duracionLlamada = duracionLlamada;
	}

}
