package co.com.callcenter.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ROL_EMPLEADO")
public class RolEmpleado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "nivel_responsabilidad")
	private Long nivelResponsabilidad;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getNivelResponsabilidad() {
		return nivelResponsabilidad;
	}

	public void setNivelResponsabilidad(Long nivelResponsabilidad) {
		this.nivelResponsabilidad = nivelResponsabilidad;
	}

}
