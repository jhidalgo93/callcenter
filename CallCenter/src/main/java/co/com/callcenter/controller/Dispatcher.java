package co.com.callcenter.controller;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.callcenter.conf.LlamadaConf;
import co.com.callcenter.model.Empleado;
import co.com.callcenter.model.Llamada;
import co.com.callcenter.service.EmpleadoService;
import co.com.callcenter.service.LlamadaService;

@Component
public class Dispatcher implements Runnable {

	@Autowired
	private LlamadaConf llamadaConf;

	@Autowired
	private LlamadaService llamadaService;

	@Autowired
	private EmpleadoService empleadoService;

	private boolean iniciarAatencion;

	private List<Empleado> empleadosDisponibles = null;

	private static final Logger LOGGER = LoggerFactory.getLogger(Dispatcher.class);

	private List<Llamada> llamadasEnEspera = null;

	@Override
	public void run() {
		while (iniciarAatencion) {
			empleadosDisponibles = empleadoService.buscarEmpleadosDisponibles();
			if (empleadosDisponibles != null && !empleadosDisponibles.isEmpty()) {
				// Invocamos el metodo que se encarga de procesar la llamada
				dispatchCall();
			}
		}

	}

	/**
	 * Metodo encargado de procesar la llamada
	 * @author Juan Carlos Hidalgo Polo
	 */
	private void dispatchCall() {
		// validamos si se encontro empleados disponibles
		if (empleadosDisponibles != null && !empleadosDisponibles.isEmpty()) {
			llamadasEnEspera = buscarLlamadasEnEspera(empleadosDisponibles);
			if (llamadasEnEspera != null && !llamadasEnEspera.isEmpty()) {

				for (Iterator<Llamada> llamadaIter = llamadasEnEspera.listIterator(); llamadaIter.hasNext();) {
					Llamada llamada = llamadaIter.next();

					// Se debe filtrar el usuario el cual puede atender la llamada
					Empleado empleadoDisponible = empleadoService.filtrarEmpleadoDisponible(empleadosDisponibles);

					// Indicamos que iniciamos un proceso de atencion a la llamada
					LOGGER.info("SE INICIA UN PROCESO DE ATENCION LA LLAMADA POR EL USUARIO: "
							+ empleadoDisponible.getNombre() + " PARA EL NUMERO:" + llamada.getTelefono());

					// removemos el empleado de la lista de disponibles
					empleadosDisponibles
							.removeIf(empleadoEliminar -> empleadoEliminar.getId().equals(empleadoDisponible.getId()));

					// atendemos la llamada
					empleadoService.atenderLlamada(empleadoDisponible, llamada);
					// removemos la llamada que en proceso de atender
					llamadaIter.remove();

				}

			}
		}

	}

	/**
	 * Metodo encargado de buscar llamadas en estado espera
	 * @author Juan Carlos Hidalgo Polo
	 * @param empleadosDisponibles
	 * @return
	 */
	private List<Llamada> buscarLlamadasEnEspera(List<Empleado> empleadosDisponibles) {
		// Obtenemos las posibles llamadas en espera
		return llamadaService.buscarLlamadaEnEspera(llamadaConf.getMaximo_de_atenciones(), empleadosDisponibles.size());
	}

	public boolean isIniciarAatencion() {
		return iniciarAatencion;
	}

	public void setIniciarAatencion(boolean iniciarAatencion) {
		this.iniciarAatencion = iniciarAatencion;
	}

}
