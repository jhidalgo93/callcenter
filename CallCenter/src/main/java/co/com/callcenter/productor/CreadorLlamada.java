package co.com.callcenter.productor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import co.com.callcenter.conf.LlamadaConf;
import co.com.callcenter.model.Llamada;
import co.com.callcenter.service.LlamadaService;
import co.com.callcenter.util.EstadoLlamadaEnum;

/**
 * Componente que simula creación de llamadas al callcenter
 * 
 * @author Juan Carlos Hidalgo
 */
@Component
public class CreadorLlamada {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreadorLlamada.class);

	@Autowired
	private LlamadaService llamadaService;

	@Autowired
	private LlamadaConf llamadaConf;

	/**
	 * Metodo encargado de crear lista de llamadas cada 3 segundos
	 * 
	 * @author Juan Carlos Hidalgo
	 */
	@Scheduled(fixedDelay = 3000)
	public void crearLLamada() {

		LOGGER.info("Se inicia proceso creacion de llamadas");
		List<Llamada> llamadasEntrantes = new ArrayList<Llamada>();
		for (int i = 0; i < llamadaConf.getNumero_acrear(); i++) {
			Llamada llamada = new Llamada();
			llamada.setEstado(EstadoLlamadaEnum.EN_ESPERA.getId());
			llamada.setFechaIngreso(new Date());
			llamada.setTelefono("32111" + i);
			llamadasEntrantes.add(llamada);
		}

		llamadaService.crearLLamadas(llamadasEntrantes);

	}

}
