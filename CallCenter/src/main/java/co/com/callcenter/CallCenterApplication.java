package co.com.callcenter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import co.com.callcenter.controller.Dispatcher;

/**
 * Clase madre de la aplicación por la cual va iniciar todo
 * 
 * @author Juan Carlos Hidalgo Polo
 *
 */
@SpringBootApplication
@EnableJpaRepositories
@EnableScheduling
public class CallCenterApplication implements CommandLineRunner {

	@Autowired
	Dispatcher dispatcher;

	public static void main(String[] args) {
		SpringApplication.run(CallCenterApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		dispatcher.setIniciarAatencion(true);
		dispatcher.run();

	}
}
