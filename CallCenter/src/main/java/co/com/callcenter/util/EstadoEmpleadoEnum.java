package co.com.callcenter.util;

public enum EstadoEmpleadoEnum {

	LIBRE(1L), OCUPADO(2L);

	private Long id;

	EstadoEmpleadoEnum(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
