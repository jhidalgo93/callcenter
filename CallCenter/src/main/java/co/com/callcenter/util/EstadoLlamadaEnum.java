package co.com.callcenter.util;

public enum EstadoLlamadaEnum {

	EN_ESPERA(1L, "EN ESPERA"), EN_PROGRESO(2L, "EN PROGRESO"), FINALIZADA(3L, "FINALIZADA");

	private Long id;
	private String descripcion;

	EstadoLlamadaEnum(Long id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
