package co.com.callcenter.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Clase encargada de configuraciones Async
 * 
 * @author Juan Carlos Hidalgo Polo
 *
 */
@Configuration
@EnableAsync
public class AsyncConf {

	/**
	 * Aarchivo de propiedades
	 */
	@Autowired
	private LlamadaConf llamadaConf;

	/**
	 * Metodo que configura el ThreadPoolTaskExecutor en donde definimos el maximo
	 * de grupos y hilos a crear en los metodos async de la aplicación
	 * 
	 * @author Juan Carlos Hidalgo Polo
	 * @return TaskExecutor
	 */
	@Bean
	@Primary
	public TaskExecutor threadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(llamadaConf.getMaximo_de_atenciones());
		executor.setMaxPoolSize(llamadaConf.getMaximo_de_atenciones());
		executor.setThreadNamePrefix("task_executor");
		executor.initialize();

		return executor;
	}

}
