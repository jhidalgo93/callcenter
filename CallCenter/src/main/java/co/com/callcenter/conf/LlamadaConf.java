package co.com.callcenter.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Clase que representa las configuraciones para las llamadas Estas estan
 * definidas en el yml archivo de propiedades principal
 * 
 * @author Juan Carlos Hidalgo Polo
 *
 */
@Configuration
@ConfigurationProperties("llamada")
public class LlamadaConf {

	private int numero_acrear;
	private int maximo_de_atenciones;
	private int duracion_minima;
	private int duracion_maxima;

	public int getNumero_acrear() {
		return numero_acrear;
	}

	public void setNumero_acrear(int numero_acrear) {
		this.numero_acrear = numero_acrear;
	}

	public int getMaximo_de_atenciones() {
		return maximo_de_atenciones;
	}

	public void setMaximo_de_atenciones(int maximo_de_atenciones) {
		this.maximo_de_atenciones = maximo_de_atenciones;
	}

	public int getDuracion_minima() {
		return duracion_minima;
	}

	public void setDuracion_minima(int duracion_minima) {
		this.duracion_minima = duracion_minima;
	}

	public int getDuracion_maxima() {
		return duracion_maxima;
	}

	public void setDuracion_maxima(int duracion_maxima) {
		this.duracion_maxima = duracion_maxima;
	}

}
