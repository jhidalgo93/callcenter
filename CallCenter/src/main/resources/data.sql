INSERT INTO ROL_EMPLEADO (id,descripcion,nivel_responsabilidad) values(1,'Operador',1);
INSERT INTO ROL_EMPLEADO (id,descripcion,nivel_responsabilidad) values(2,'Supervisor',2);
INSERT INTO ROL_EMPLEADO (id,descripcion,nivel_responsabilidad) values(3,'Director',3);


INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (1, 'Pepito', 'C109493566',1,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (2, 'Marcos', 'C109493563',1,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (3, 'Tulio', 'C109493562',1,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (4, 'Julieta', 'C109491566',1,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (5, 'Monica', 'C109693566',1,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (6, 'Polo', 'C129493566',2,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (7, 'Chavo', 'C159493566',2,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (8, 'Sirena', 'C129493566',2,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (9, 'Merlin', 'C159493566',2,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (10, 'Toto', 'C129493566',2,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (11, 'Mexico', 'C159493566',2,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (12, 'Milena', 'C109467566',3,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (13, 'Humberto', 'C108893566',3,1);
INSERT INTO EMPLEADO (id, nombre, identificacion, rol,estado) values (14, 'Sergio', 'C100493566',3,1);

